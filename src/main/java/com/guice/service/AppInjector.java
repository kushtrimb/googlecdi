package com.guice.service;

import com.google.inject.AbstractModule;

/**
 * Created by Kushtrim on 5/13/2017.
 */
public class AppInjector extends AbstractModule {

    @Override
    protected void configure() {
        bind(MessageService.class).to(MyService.class);
    }

}
