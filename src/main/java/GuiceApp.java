import com.google.inject.Inject;
import com.guice.service.MessageService;

/**
 * Created by Kushtrim on 5/13/2017.
 */
public class GuiceApp {

    private MessageService service;

    @Inject
    public void setService(MessageService svc){
        this.service=svc;
    }

    public boolean sendMessage(String msg, String rec){
        return service.sendMessage(msg, rec);
    }
}
